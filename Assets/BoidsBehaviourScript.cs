﻿#define MULTITHREADING

#if MULTITHREADING
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using Unity.Collections;
using UnityEngine.Jobs;

public class BoidsBehaviourScript : MonoBehaviour
{
    private const uint finalInstanceNumber = 512;

    [SerializeField]
    // Initialize to make compiler happy
    private GameObject boidsAgentPrefab = null;

    private bool instantiationInProgress = true;

    private NativeArray<Boid> inputBoids, outputBoids;
    private TransformAccessArray transforms;
    private Unity.Jobs.JobHandle? previousJobHandle;

    private struct BoidsAgentJob : IJobParallelForTransform
    {
        [ReadOnly]
        public float deltaTime;

        [ReadOnly]
        public uint boidsCount;

        [ReadOnly]
        public NativeArray<Boid> inputBoids;

        [WriteOnly]
        public NativeArray<Boid> outputBoids;

        public void Execute(int index, TransformAccess transform)
        {
            Boid result
                = inputBoids[index].Update(inputBoids, deltaTime, boidsCount);
            transform.position = result.position;
            outputBoids[index] = result;
        }
    }

    private struct Boid
    {
        public Vector3 velocity, position;

        public Boid(Vector3 position)
        {
            this.position = position;
            this.velocity = Vector3.zero;
        }

        public Boid(Vector3 position, Vector3 velocity)
        {
            this.position = position;
            this.velocity = velocity;
        }


        public Boid Update(
                NativeArray<Boid> boids, float deltaTime, uint boidsCount)
        {
            const float maximalSpeed = 1;
            Vector3 new_velocity
                =
                Vector3.ClampMagnitude(
                        velocity + Acceleration(boids, boidsCount) * deltaTime,
                        maximalSpeed);
            return new Boid(position + new_velocity * deltaTime, new_velocity);
        }


        private ICollection<uint> Neighbors(
                NativeArray<Boid> boids, uint boidsCount) {
            List<uint> neighbors = new List<uint>();
            const int neighborsRadius = 8;
            for (uint i = 0; i < boidsCount; ++i)
                if (
                        Vector3.Distance(boids[(int)i].position, position)
                        < neighborsRadius)
                    neighbors.Add(i);
            return neighbors;
        }


        private Vector3 Acceleration(NativeArray<Boid> boids, uint boidsCount)
        {
            ICollection<uint> neighbors = Neighbors(boids, boidsCount);
            if (neighbors.Count == 0)
                return Vector3.zero;
            return
                Alligment(boids, neighbors)
                + Cohesion(boids, neighbors)
                + Separation(boids, neighbors);
        }


        private Vector3 Alligment(
                NativeArray<Boid> boids, ICollection<uint> neighbors)
        {
            return
                (
                 from neighbor in neighbors
                 select boids[(int)neighbor].velocity)
                .Aggregate((accumulated, current) => accumulated + current)
                / neighbors.Count;
        }


        // Move to center of mass
        private Vector3 Cohesion(
                NativeArray<Boid> boids, ICollection<uint> neighbors)
        {
            return
                (
                 from neighbor in neighbors
                 select boids[(int)neighbor].position)
                .Aggregate(
                        (accumulated, current) => accumulated + current)
                / neighbors.Count
                - position;
        }


        // Don't get close to other objects
        private Vector3 Separation(
                NativeArray<Boid> boids, ICollection<uint> neighbors)
        {
            Vector3 sum = Vector3.zero;
            foreach (uint neighbor in neighbors)
            {
                Vector3 neighborPosition = boids[(int)neighbor].position;
                float distance
                    = Vector3.Distance(position, neighborPosition);
                sum
                    +=
                    (
                     distance == 0
                     ? Vector3.zero 
                     :
                     (position - neighborPosition)
                     / (distance * distance));
            }
            return sum;
        }
    }

    
    private void Start()
    {
        inputBoids
        = new NativeArray<Boid>((int)finalInstanceNumber, Allocator.Persistent);
        outputBoids
        = new NativeArray<Boid>((int)finalInstanceNumber, Allocator.Persistent);
        transforms = new TransformAccessArray((int)finalInstanceNumber);
    }

    // Update is called once per frame
    private void Update()
    {
        void Swap<T>(ref T a, ref T b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }
        

        const float instantiationTime = 0.1f, maxInstanciationDist = 1;
        const int delay = 1;

        if (previousJobHandle != null)
            previousJobHandle.Value.Complete();
        Unity.Jobs.JobHandle jobHandle
            = new BoidsAgentJob
            {
                deltaTime = Time.deltaTime,
                boidsCount = (uint)transforms.length,
                inputBoids = inputBoids,
                outputBoids = outputBoids
            }
        .Schedule(transforms);

        if (instantiationInProgress)
        {
            while (Time.time > delay + transforms.length * instantiationTime)
            {
                transforms.Add(
                    Instantiate(
                            boidsAgentPrefab,
                            transform.position
                            +
                            new Vector3(
                                Random.Range(
                                    -maxInstanciationDist,
                                    maxInstanciationDist),
                                Random.Range(
                                    -maxInstanciationDist,
                                    maxInstanciationDist),
                                Random.Range(
                                    -maxInstanciationDist,
                                    maxInstanciationDist)),
                            transform.rotation).transform);
                outputBoids[transforms.length - 1]
                    = new Boid(transforms[transforms.length - 1].position);
                if (transforms.length== finalInstanceNumber)
                {
                    instantiationInProgress = false;
                    break;
                }
            }
        }
        if (previousJobHandle != null)
            previousJobHandle.Value.Complete();
        previousJobHandle = jobHandle;
        Swap(ref inputBoids, ref outputBoids);
    }


    private void OnDestroy()
    {
        if (previousJobHandle != null)
            previousJobHandle.Value.Complete();
        transforms.Dispose();
        inputBoids.Dispose();
        outputBoids.Dispose();
    }
}

#else // Single thread version

using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class BoidsBehaviourScript : MonoBehaviour
{
    private const uint finalInstanceNumber = 512;

    [SerializeField]
    // Initialize to make compiler happy
    private GameObject boidsAgentPrefab = null;

    private bool instantiationInProgress = true;
    private uint boidsCount = 0;
    private Transform[] transforms = new Transform[finalInstanceNumber];
    private Boid[] boids = new Boid[finalInstanceNumber];

    private struct Boid
    {
        public Vector3 velocity, position;

        public Boid(Vector3 position)
        {
            this.position = position;
            this.velocity = Vector3.zero;
        }


        public Boid(Vector3 position, Vector3 velocity)
        {
            this.position = position;
            this.velocity = velocity;
        }


        public void Update(Boid[] boids, float deltaTime, uint boidsCount)
        {
            const float maximalSpeed = 1;
            velocity
                =
                Vector3.ClampMagnitude(
                        velocity + Acceleration(boids, boidsCount) * deltaTime,
                        maximalSpeed);
            position += velocity * deltaTime;
        }


        private ICollection<uint> Neighbors(Boid[] boids, uint boidsCount) {
            List<uint> neighbors = new List<uint>();
            const int neighborsRadius = 8;
            for (uint i = 0; i < boidsCount; ++i)
                if (
                        Vector3.Distance(boids[(int)i].position, position)
                        < neighborsRadius)
                    neighbors.Add(i);
            return neighbors;
        }


        private Vector3 Acceleration(Boid[] boids, uint boidsCount)
        {
            ICollection<uint> neighbors = Neighbors(boids, boidsCount);
            if (neighbors.Count == 0)
                return Vector3.zero;
            return
                Alligment(boids, neighbors)
                + Cohesion(boids, neighbors)
                + Separation(boids, neighbors);
        }


        private Vector3 Alligment(Boid[] boids, ICollection<uint> neighbors)
        {
            return
                (
                 from neighbor in neighbors
                 select boids[(int)neighbor].velocity)
                .Aggregate((accumulated, current) => accumulated + current)
                / neighbors.Count;
        }


        // Move to center of mass
        private Vector3 Cohesion(Boid[] boids, ICollection<uint> neighbors)
        {
            return
                (
                 from neighbor in neighbors
                 select boids[(int)neighbor].position)
                .Aggregate(
                        (accumulated, current) => accumulated + current)
                / neighbors.Count
                - position;
        }


        // Don't get close to other objects
        private Vector3 Separation(Boid[] boids, ICollection<uint> neighbors)
        {
            Vector3 sum = Vector3.zero;
            foreach (uint neighbor in neighbors)
            {
                Vector3 neighborPosition = boids[(int)neighbor].position;
                float distance
                    = Vector3.Distance(position, neighborPosition);
                sum
                    +=
                    (
                     distance == 0
                     ? Vector3.zero 
                     :
                     (position - neighborPosition)
                     / (distance * distance));
            }
            return sum;
        }
    }

    
    // Update is called once per frame
    private void Update()
    {
        const float instantiationTime = 0.1f, maxInstanciationDist = 1;
        const int delay = 1;

        for (uint i = 0; i < boidsCount; ++i)
        {
            boids[i].Update(boids, Time.deltaTime, boidsCount);
            transforms[i].position = boids[i].position;
        }

        if (instantiationInProgress)
        {
            while (Time.time > delay + boidsCount * instantiationTime)
            {
                transforms[(int)boidsCount] = 
                    Instantiate(
                            boidsAgentPrefab,
                            transform.position
                            +
                            new Vector3(
                                Random.Range(
                                    -maxInstanciationDist,
                                    maxInstanciationDist),
                                Random.Range(
                                    -maxInstanciationDist,
                                    maxInstanciationDist),
                                Random.Range(
                                    -maxInstanciationDist,
                                    maxInstanciationDist)),
                            transform.rotation).transform;
                boids[(int)boidsCount]
                    = new Boid(transforms[(int)boidsCount].position);
                if (++boidsCount== finalInstanceNumber)
                {
                    instantiationInProgress = false;
                    break;
                }
            }
        }
    }
}

#endif // Single thread version
